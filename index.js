// Mock Database
let posts = [];
// Movie post ID (first id of created movie) - can be unique property
let count = 1;

// Add post data
// direct target element 
document.querySelector('#form-add-post').addEventListener('submit' , (e) => {
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});
	// to increment value of the count
	count++;
// invoke showposts
	showPosts(posts);
	alert('Movie Post successfully added');
})

// Show posts
const showPosts = (posts) => {
	// will handle the movie post entries
	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
			<div id = "post-${post.id}">
				<h3 id = "post-title-${post.id}">${post.title}</h3>
				<p id = "post-body-${post.id}">${post.body}</p>
				<button onclick = "editPost('${post.id}')">Edit</button>
				<button onclick = "deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})
	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Edit post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
}

// Update post
// target editing element
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	/*
		i = 0 , posts.length = 2
		posts[0] = 1 === '#txt-edit-id = 2 //not equal 
		posts[1] = 2 === '#txt-edit-id = 2 // equal 
	*/

	// i = represent index
	for (let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			// assigning value
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('successfully updated');

			break
		}
	}
})

// Delete post
const deletePost = (id) => {
	posts = posts.filter(post => post.id.toString() !== id)
	
	showPosts(posts);
	alert('Post Deleted')
}





